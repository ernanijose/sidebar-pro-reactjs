import { ProSidebar, SidebarHeader, SidebarFooter, SidebarContent, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import { Link } from 'react-router-dom';
import * as FaIcon from 'react-icons/fa';
import 'react-pro-sidebar/dist/css/styles.css';
import './App.css';

function App() {
  return (
    /*
    <ProSidebar>
      <Menu iconShape='square'>
        <MenuItem icon={<FaIcon.FaGem />}>Dashboard</MenuItem>
        <SubMenu title="Components" icon={<FaIcon.FaHeart />}>
          <MenuItem>Components 1</MenuItem>
          <MenuItem>Components 2</MenuItem>
        </SubMenu>
      </Menu>
    </ProSidebar>
    */
   <ProSidebar>
     <SidebarHeader>
      <Menu iconShape='square'>
        <MenuItem icon={<FaIcon.FaGem />}>
        Dashboard        
        </MenuItem>        
      </Menu>
     </SidebarHeader>
     <SidebarContent>
     <Menu iconShape='square'>        
        <SubMenu title="Components" icon={<FaIcon.FaHeart />}>
          <MenuItem>Components 1</MenuItem>
          <MenuItem>Components 2</MenuItem>
        </SubMenu>
      </Menu>
     </SidebarContent>
   </ProSidebar>
  );
}

export default App;
